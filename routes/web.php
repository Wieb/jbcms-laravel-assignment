<?php

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', function () {
        return redirect()->route('home');
    });
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/users', 'UsersController@show')->name('users.show');
    Route::post('/users/create', 'UsersController@create')->name('users.create');
    Route::post('/users/delete', 'UsersController@delete')->name('users.delete');
});
