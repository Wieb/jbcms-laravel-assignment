<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $users = User::all();
        return view('users', [
            'route' => \Request::route()->getName(),
            'users' => $users
        ]);
    }

    public function create()
    {
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        User::create([
            'id' => uniqid(),
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);

        return redirect()->back();
    }

    public function delete()
    {
        $data = request()->validate([
            'user_id' => ['required'],
        ]);

        if (auth()->user()->id == $data['user_id']) return redirect()->back();

        User::findOrFail($data['user_id'])->delete();

        return redirect()->back();
    }
}
