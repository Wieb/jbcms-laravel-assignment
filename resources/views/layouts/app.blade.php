<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        @switch($route)
            @case('home')
                Home -
                @break
            @case('users.show')
                Users -

        @endswitch
        Jbcms</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="icon" href="{{ 'img/favicon.ico' }}">
</head>
<body>
    <div class="nav">
        <h1 class="nav__title">Jbcms</h1>
        <section class="section">
            <h2 class="section__title">travel</h2>
            <a href="{{ route('home') }}" class="section__item @if($route == 'home') route--active @endif"><img class="section__item__icon" src="{{ asset('img/home.png') }}" alt="item image">Home</a>
            <a href="{{ route('users.show') }}" class="section__item @if($route == 'users.show') route--active @endif"><img class="section__item__icon" src="{{ asset('img/users.png') }}" alt="item image">Users</a>
        </section>

        <section class="section">
            <h2 class="section__title">account</h2>
            <span onclick="logout()" class="section__item">logout</span>
        </section>
    </div>

    <div class="content">
        <div class="content__header">
            @yield('content__header')
        </div>
        @yield('content')
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</body>
    <script type="text/javascript">
        function logout () {
            event.preventDefault()
            document.querySelector('#logout-form').submit()
        }
    </script>
    @yield('scripts')
</html>
