<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login - jbcms</title>
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
</head>
<body>
    <div class="login">
        <h1 class="login__title">Login to<br><span class="bold">Jbcms</span></h1>
        <form method="POST" action="{{ route('login') }}" class="login__form">
            @csrf

            <label for="email" class="login__name">Email</label>
            <input id="email" type="email" class="login__input @error('email') is-invalid @enderror" name="email" value="" required autocomplete="email" autofocus>
            @error('email')
            <span class="login__error" role="alert">
                {{ $message }}
            </span>
            @enderror

            <label for="password" class="login__name">Password</label>
            <input id="password" type="password" class="login__input @error('password') is-invalid @enderror" name="password" required autocomplete="off">
            @error('password')
            <span class="login__error" role="alert">
                {{ $message }}
            </span>
            @enderror

            <button type="submit" class="login__submit">
                Login to cms
            </button>
        </form>
    </div>
</body>
</html>
