@extends('layouts.app')

@section('content__header')
    <h1 class="content__header__title">Home</h1>
@endsection

@section('content')
    <div class="home">
        <h1 class="home__title"><span class="bold">Jbcms</span></h1>
        <p class="home__description">Welcome to Jbcms. In this cms you can create, search and destroy users. <br>Have fun!</p>
        <a href="{{ route('users.show') }}"><button class="button button--next">Go to users</button></a>
    </div>
@endsection
