@extends('layouts.app')

@section('content__header')
    <h1 class="content__header__title">Users</h1>
    <button type="button" class="button button--create" onclick="updateCreateDisplay()">Create User</button>
@endsection

@section('content')
    <div class="users">
        <h2 class="users__title">Users list</h2>
        <div class="list">
            @foreach ( $users as $user )
            <div class="list__item">
                <h3 class="list__item__name">{{ $user->name }}</h3>
                <ul class="list__item__content">
                    <li>Id: <span class="default">{{ $user->id }}</span></li>
                    <li>Email: <span class="default">{{ $user->email }}</span></li>
                    <li>Password: <span class="default">****</span></li>
                </ul>

                @if(auth()->user()->id !== $user->id)
                <form action="{{ route('users.delete') }}" method="post" class="list__item__form">
                    @csrf
                    <input type="text" style="display: none" name="user_id" value="{{ $user->id }}">
                    <button type="submit" class="list__item__delete">
                        <img src="{{ asset('img/trashcan.png') }}" alt="" class="list__item__delete__img">
                    </button>
                </form>
                @else
                <span class="list__item__you">You</span>
                @endif
            </div>
            @endforeach

            @for ( $i = 0; $i < (6 - count($users)); $i++ )
                <div class="list__item blank"></div>
            @endfor
        </div>

        <div class="create">
            <form action="{{ route('users.create') }}" method="post" class="create__form">
                @csrf
                <div class="create__form__return">
                    <div class="create__form__return__cross" onclick="updateCreateDisplay()">
                        <div></div>
                        <div></div>
                    </div>
                </div>

                <h2 class="create__form__title">Create new user</h2>

                <div class="create__form__box">
                    <label for="name" class="create__form__name">Name</label>

                    <input id="name" type="text" class="create__form__input @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="off" autofocus>

                    @error('name')
                        <span class="create__form__error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="create__form__box">
                    <label for="email" class="create__form__name">Email</label>
                    <input id="email" type="text" class="create__form__input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus>
                    @error('email')
                        <span class="create__form__error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="create__form__box">
                    <label for="password" class="create__form__name">Password</label>
                    <input id="password" type="password" class="create__form__input @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="off" autofocus>
                    @error('password')
                        <span class="create__form__error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <button type="submit" class="button button--submit">Create user</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    let create = document.querySelector('.create')
    function updateCreateDisplay() {
        if (create.style.display === 'flex') {
            create.style.display = 'none'
            document.querySelectorAll('.create input').forEach(e => {
                e.value = ''
            })
            return
        }
        create.style.display = 'flex'
    }
</script>
@endsection
